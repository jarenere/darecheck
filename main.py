import typer
from selenium import webdriver
from selenium.webdriver.common.by import By
import requests
from urllib.parse import urlparse
from datetime import date
import time


class my_driver():
    def __init__(self, implicitly_wait: int = 0, headless: bool = True):
        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        options.headless = headless
        options.add_argument('--disable-gpu')
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("--window-size=1600,900")
        self.driver = webdriver.Chrome(options=options)
        self.driver.implicitly_wait(implicitly_wait)

    def __enter__(self):
        return self.driver

    def __exit__(self, type, value, tb):
        self.driver.close()


def is_holiday(holidays: list[dict], date: date) -> bool:
    if date.weekday() > 4:
        return True
    for i in holidays:
        if i['day'] == str(date.day) and i['month'] == str(date.month) and i['year'] == str(date.year):
            return True
    return False


def login(driver: webdriver.Chrome, email: str, password: str):
    driver.get("https://darefichajes.azurewebsites.net/")
    email_input = driver.find_element(By.XPATH, "//input[@type='email'][@name='loginfmt']")
    email_input.send_keys(f'{email}\n')
    # due to js code...
    time.sleep(1)
    pass_input = driver.find_element(By.XPATH, "//input[@type='password'][@name='passwd']")
    pass_input.send_keys(f'{password}\n')
    # form with you want to stay logged in
    logged_in = driver.find_element(By.XPATH, "//input[@type='submit']")
    logged_in.click()
    print('Successfully logged in!')


def setup_session(cookies: list[dict]) -> requests.session:
    s = requests.Session()
    for cookie in cookies:
        s.cookies.set(cookie['name'], cookie['value'])
        s.headers.update({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"})
    s.headers.update({"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"})
    return s


def main(email: str = typer.Option(..., prompt=True, envvar="DARE_EMAIL"),
         password: str = typer.Option(..., prompt=True,  hide_input=True, envvar="DARE_PASSWORD"),
         headless: bool = typer.Option(True, envvar="DARE_HEADLESS", help="Browser in headless mode")):
    """
    Tool to automate the daily check-in in Dare Planet Corp.
    """
    print(email, password, headless)
    with my_driver(20, headless) as driver:
        login(driver, email, password)
        cookies = driver.get_cookies()
        daily_check_in_url = driver.find_element(By.ID, "btnDailyCheckInId").get_attribute("href")

    user_id = urlparse(daily_check_in_url).query.split("=")[1]
    s = setup_session(cookies)
    re = s.post('https://darefichajes.azurewebsites.net/Holidays/LoadHolidaysCalendar',
                data=f'year=2022&idEmployee={user_id}')
    if is_holiday(re.json()['result'], date.today()):
        print("holidays!")
    else:
        s.get(daily_check_in_url)
        print("checkin!")


if __name__ == "__main__":
    typer.run(main)
