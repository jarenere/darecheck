# Darecheck

Tool to automate the daily check-in in Dare Planet Corp.

## Getting started

```
pip install -r requeriments
# time to play 
python main.py --help
```

**Usage**:

```console
$ python main.py [OPTIONS]
```

**Options**:

* `--email TEXT`: [env var: DARE_EMAIL; required]
* `--password TEXT`: [env var: DARE_PASSWORD; required]
* `--headless / --no-headless`: Browser in headless mode  [env var: DARE_HEADLESS; default: True]
* `--install-completion`: Install completion for the current shell.
* `--show-completion`: Show completion for the current shell, to copy it or customize the installation.
* `--help`: Show this message and exit.